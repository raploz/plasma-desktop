# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Phu Hung Nguyen <phu.nguyen@kdemail.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-11 02:03+0000\n"
"PO-Revision-Date: 2021-05-26 21:20+0200\n"
"Last-Translator: Phu Hung Nguyen <phu.nguyen@kdemail.net>\n"
"Language-Team: Vietnamese <kde-l10n-vi@kde.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 20.08.1\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Chung"

#: package/contents/ui/code/tools.js:42
#, kde-format
msgid "Remove from Favorites"
msgstr "Xoá khỏi \"Ưa thích\""

#: package/contents/ui/code/tools.js:46
#, kde-format
msgid "Add to Favorites"
msgstr "Thêm vào \"Ưa thích\""

#: package/contents/ui/code/tools.js:70
#, kde-format
msgid "On All Activities"
msgstr "Ở tất cả các Hoạt động"

#: package/contents/ui/code/tools.js:120
#, kde-format
msgid "On the Current Activity"
msgstr "Ở Hoạt động hiện tại"

#: package/contents/ui/code/tools.js:134
#, kde-format
msgid "Show in Favorites"
msgstr "Hiện trong \"Ưa thích\""

#: package/contents/ui/ConfigGeneral.qml:46
#, kde-format
msgid "Icon:"
msgstr "Biểu tượng:"

#: package/contents/ui/ConfigGeneral.qml:126
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Chọn…"

#: package/contents/ui/ConfigGeneral.qml:131
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Clear Icon"
msgstr "Đặt lại biểu tượng"

#: package/contents/ui/ConfigGeneral.qml:149
#, kde-format
msgid "Show applications as:"
msgstr "Hiện ứng dụng thành:"

#: package/contents/ui/ConfigGeneral.qml:151
#, kde-format
msgid "Name only"
msgstr "Chỉ tên"

#: package/contents/ui/ConfigGeneral.qml:151
#, kde-format
msgid "Description only"
msgstr "Chỉ mô tả"

#: package/contents/ui/ConfigGeneral.qml:151
#, kde-format
msgid "Name (Description)"
msgstr "Tên (mô tả)"

#: package/contents/ui/ConfigGeneral.qml:151
#, kde-format
msgid "Description (Name)"
msgstr "Mô tả (tên)"

#: package/contents/ui/ConfigGeneral.qml:161
#, kde-format
msgid "Behavior:"
msgstr "Ứng xử:"

#: package/contents/ui/ConfigGeneral.qml:163
#, kde-format
msgid "Sort applications alphabetically"
msgstr "Sắp xếp ứng dụng theo bảng chữ cái"

#: package/contents/ui/ConfigGeneral.qml:171
#, kde-format
msgid "Flatten sub-menus to a single level"
msgstr "Làm phẳng các trình đơn con thành chỉ còn một cấp"

#: package/contents/ui/ConfigGeneral.qml:179
#, kde-format
msgid "Show icons on the root level of the menu"
msgstr "Hiện biểu tượng ở cấp gốc của trình đơn"

#: package/contents/ui/ConfigGeneral.qml:189
#, kde-format
msgid "Show categories:"
msgstr "Hiện các mục:"

#: package/contents/ui/ConfigGeneral.qml:192
#, kde-format
msgid "Recent applications"
msgstr "Ứng dụng gần đây"

#: package/contents/ui/ConfigGeneral.qml:193
#, kde-format
msgid "Often used applications"
msgstr "Ứng dụng hay dùng"

#: package/contents/ui/ConfigGeneral.qml:200
#, kde-format
msgid "Recent files"
msgstr "Tệp gần đây"

#: package/contents/ui/ConfigGeneral.qml:201
#, kde-format
msgid "Often used files"
msgstr "Tệp hay dùng"

#: package/contents/ui/ConfigGeneral.qml:207
#, kde-format
msgid "Sort items in categories by:"
msgstr "Sắp xếp các thứ trong mục theo:"

#: package/contents/ui/ConfigGeneral.qml:208
#, kde-format
msgctxt ""
"@item:inlistbox Sort items in categories by [Recently used | Often used]"
msgid "Recently used"
msgstr "Dùng gần đây"

#: package/contents/ui/ConfigGeneral.qml:208
#, kde-format
msgctxt ""
"@item:inlistbox Sort items in categories by [Recently used | Ofetn used]"
msgid "Often used"
msgstr "Hay dùng"

#: package/contents/ui/ConfigGeneral.qml:218
#, kde-format
msgid "Search:"
msgstr "Tìm kiếm:"

#: package/contents/ui/ConfigGeneral.qml:220
#, kde-format
msgid "Expand search to bookmarks, files and emails"
msgstr "Mở rộng tìm kiếm ra cả dấu nhớ, tệp và thư"

#: package/contents/ui/ConfigGeneral.qml:228
#, kde-format
msgid "Align search results to bottom"
msgstr "Căn kết quả tìm kiếm về cuối"

#: package/contents/ui/DashboardRepresentation.qml:290
#, kde-format
msgid "Searching for '%1'"
msgstr "Đang tìm '%1'"

#: package/contents/ui/DashboardRepresentation.qml:290
#, fuzzy, kde-format
#| msgid "Type to search…"
msgctxt "@info:placeholder as in, 'start typing to initiate a search'"
msgid "Type to search…"
msgstr "Gõ để tìm kiếm…"

#: package/contents/ui/DashboardRepresentation.qml:390
#, kde-format
msgid "Favorites"
msgstr "Ưa thích"

#: package/contents/ui/DashboardRepresentation.qml:608
#: package/contents/ui/DashboardTabBar.qml:42
#, kde-format
msgid "Widgets"
msgstr "Phụ kiện"

#: package/contents/ui/DashboardTabBar.qml:31
#, kde-format
msgid "Apps & Docs"
msgstr "Ứng dụng & tài liệu"

#: package/contents/ui/main.qml:250
#, kde-format
msgid "Edit Applications…"
msgstr "Sửa ứng dụng…"

#~ msgid "Recent contacts"
#~ msgstr "Liên hệ gần đây"

#~ msgid "Often used contacts"
#~ msgstr "Liên hệ hay dùng"

#~ msgid "Search…"
#~ msgstr "Tìm kiếm…"

#~ msgid "Search..."
#~ msgstr "Tìm kiếm…"
