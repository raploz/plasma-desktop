# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Victor Ibragimov <victor.ibragimov@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-22 02:07+0000\n"
"PO-Revision-Date: 2019-09-17 14:18+0500\n"
"Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>\n"
"Language-Team: English <kde-i18n-doc@kde.org>\n"
"Language: tg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.3\n"

#: applicationmodel.cpp:67
#, kde-format
msgid "Other…"
msgstr ""

#. i18n: ectx: label, entry (browserApplication), group (General)
#: browser_settings.kcfg:9
#, kde-format
msgid "Default web browser"
msgstr ""

#: components/componentchooserarchivemanager.cpp:15
#, kde-format
msgid "Select default archive manager"
msgstr ""

#: components/componentchooserbrowser.cpp:15
#, kde-format
msgid "Select default browser"
msgstr ""

#: components/componentchooseremail.cpp:20
#, kde-format
msgid "Select default e-mail client"
msgstr ""

#: components/componentchooserfilemanager.cpp:15
#, kde-format
msgid "Select default file manager"
msgstr ""

#: components/componentchoosergeo.cpp:13
#, kde-format
msgid "Select default map"
msgstr ""

#: components/componentchooserimageviewer.cpp:15
#, kde-format
msgid "Select default image viewer"
msgstr ""

#: components/componentchoosermusicplayer.cpp:15
#, kde-format
msgid "Select default music player"
msgstr ""

#: components/componentchooserpdfviewer.cpp:11
#, kde-format
msgid "Select default PDF viewer"
msgstr ""

#: components/componentchoosertel.cpp:17
#, kde-format
msgid "Select default dialer application"
msgstr ""

#: components/componentchooserterminal.cpp:25
#, kde-format
msgid "Select default terminal emulator"
msgstr ""

#: components/componentchoosertexteditor.cpp:15
#, kde-format
msgid "Select default text editor"
msgstr ""

#: components/componentchooservideoplayer.cpp:15
#, kde-format
msgid "Select default video player"
msgstr ""

#: ui/ComponentOverlay.qml:22
#, kde-format
msgid "Details"
msgstr ""

#: ui/ComponentOverlay.qml:28
#, kde-format
msgid ""
"This application does not advertise support for the following file types:"
msgstr ""

#: ui/ComponentOverlay.qml:42
#, kde-format
msgctxt "@action:button"
msgid "Force Open Anyway"
msgstr ""

#: ui/ComponentOverlay.qml:50
#, kde-format
msgid ""
"The following file types are still associated with a different application:"
msgstr ""

#: ui/ComponentOverlay.qml:59
#, kde-format
msgctxt "@label %1 is a MIME type and %2 is an application name"
msgid "%1 associated with %2"
msgstr ""

#: ui/ComponentOverlay.qml:65
#, kde-format
msgctxt "@action:button %1 is an application name"
msgid "Re-assign-all to %1"
msgstr ""

#: ui/ComponentOverlay.qml:73
#, kde-format
msgid "Change file type association manually"
msgstr ""

#: ui/main.qml:17
#, kde-format
msgid ""
"’%1’ seems to not support the following mimetypes associated with this kind "
"of application: %2"
msgstr ""

#: ui/main.qml:45
#, kde-format
msgctxt "Internet related application’s category’s name"
msgid "Internet"
msgstr ""

#: ui/main.qml:49
#, kde-format
msgid "Web browser:"
msgstr ""

#: ui/main.qml:71
#, kde-format
msgid "Email client:"
msgstr ""

#: ui/main.qml:93
#, kde-format
msgctxt "Default phone app"
msgid "Dialer:"
msgstr ""

#: ui/main.qml:115
#, kde-format
msgctxt "Multimedia related application’s category’s name"
msgid "Multimedia"
msgstr ""

#: ui/main.qml:120
#, kde-format
msgid "Image viewer:"
msgstr ""

#: ui/main.qml:144
#, kde-format
msgid "Music player:"
msgstr ""

#: ui/main.qml:167
#, kde-format
msgid "Video player:"
msgstr ""

#: ui/main.qml:189
#, kde-format
msgctxt "Documents related application’s category’s name"
msgid "Documents"
msgstr ""

#: ui/main.qml:194
#, kde-format
msgid "Text editor:"
msgstr ""

#: ui/main.qml:216
#, kde-format
msgid "PDF viewer:"
msgstr ""

#: ui/main.qml:238
#, kde-format
msgctxt "Utilities related application’s category’s name"
msgid "Utilities"
msgstr ""

#: ui/main.qml:243
#, kde-format
msgid "File manager:"
msgstr ""

#: ui/main.qml:265
#, kde-format
msgid "Terminal emulator:"
msgstr ""

#: ui/main.qml:278
#, kde-format
msgid "Archive manager:"
msgstr ""

#: ui/main.qml:300
#, kde-format
msgctxt "Map related application’s category’s name"
msgid "Map:"
msgstr ""

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Victor Ibragimov"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "victor.ibragimov@gmail.com"

#~ msgid "Unknown"
#~ msgstr "Номаълум"
